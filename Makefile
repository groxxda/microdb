# *DOCUMENTATION*
# To see a list of typical targets execute "make help"
# More info can be located in ./README

CC = g++
CFLAGS = -O2 -Wall -Werror 
# CXX = $(CC) 
CXXFLAGS = ${CFLAGS}
# LDFLAGS =
VALGRIND = /usr/bin/valgrind

QUIET = @
EXEC = $(QUIET)
PRINT = $(QUIET) echo make: 

SRCS = $(wildcard *.cpp util/*.cpp)
OBJS = $(SRCS:.cpp=.o)
TEST_SRCS = $(wildcard test/*.cpp)
TEST_OBJS = $(TEST_SRCS:.cpp=.o)
TESTFILE := 80mb

.PHONY: all clean

all:: compile compile-tests

# $(CC) -o db $(OBJS)

test: compile-tests ${TESTFILE}
	${PRINT} Executing tests
	./test/external_sort_test
	${PRINT} Tests done

test/external_sort_test: $(OBJS)

compile-tests: compile $(TEST_OBJS) test/external_sort_test

run: $(TARGET)
	$(PRINT) Executing $(TARGET).
	$(EXEC) ./db	
	$(PRINT) $(TARGET) returned with exit code $!. #TODO

leakcheck: $(TARGET)
	$(VALGRIND) --leak-check=full ./test/external_sort_test > /dev/null
	
compile: $(OBJS)

db: $(OBJS)

generate: $(TESTFILE)

$(TESTFILE): gen/gen
	$(EXEC) ./gen/gen ./50mb 10485760
	$(PRINT) Test data generated.

gen/gen: gen/gen.cpp gen/Makefile
	#$(EXEC) $(CXX) $(CFLAGS) gen/gen.cpp -o gen/gen
	$(MAKE) --directory=gen
	$(PRINT) Generator compiled.

gen/gen.cpp:
	$(PRINT) Downloading generator source code from www-db.in.tum.de.
	$(EXEC) test -d gen || mkdir gen
	$(EXEC) wget -q -O - http://www-db.in.tum.de/teaching/ss12/im/gen.tar.gz | tar -xz -C gen
	$(PRINT) Patching generator source code to work with gcc 4.7
	$(EXEC) patch -R gen/gen.cpp < contrib/gen_include-gcc47.patch

clean:
	$(EXEC) -rm -rf db *.o */*.o 
	$(PRINT) Cleaning done.
