/*
 * external_sort.cpp
 *
 *  Created on: Apr 23, 2012
 *      Author: dev
 */

#include <iostream>
#include <sys/mman.h>
#include <algorithm>
#include <vector>
#include <new>
#include <string.h>
#include <errno.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <fcntl.h>
#include <unistd.h>
#include <queue>

using namespace std;

struct SORTENTRY {
	int originRun;
	unsigned long value;

	bool operator < (const SORTENTRY& rhs) const {
		return value > rhs.value;
	}
};

string getOutfilename(int run) {
	stringstream ss;
	ss << "run" << run << ".out";
	return ss.str();
}

void externalSort(int fdInput, unsigned long size, int fdOutput, unsigned long memSize) {
	// check if memory is large enough for one pass (internal sort)
	cout << "sizeof(unsigned long)=" << sizeof(unsigned long) << endl;
	cout << memSize << " Bytes memory, " << size*sizeof(long) << " Bytes to sort." << endl;
	if (memSize > size * sizeof(long)) {
		// perform internal sort
		cout << "buffer is large enough, performing internal sort." << endl;
	} else {
		// perform external sort
		cout << "performing external sort" << endl;

		// partition the input
		const int runCount = (size * sizeof(long) + memSize - 1) / memSize;
		cout << "Partitioning input into " << runCount << " runs." << endl;

		// read, sort and write runs
		for (int run = 0; run < runCount; run++) {
			cout << "Reading run #" << run << "." << endl;
			int mappingSize = memSize;
			if (run == runCount - 1) {
				mappingSize = size*sizeof(unsigned long) - memSize * run;
				cout << "Last run, only reading remaining " <<mappingSize<<" Bytes."<<endl;
			}

			unsigned long *inputmm = (unsigned long *)mmap(NULL, mappingSize, PROT_WRITE, MAP_PRIVATE, fdInput, run * memSize);

			if (inputmm == MAP_FAILED) {
				cerr << "Cannot map input file to memory." << endl;
				return;
			}

			// sort using std::sort
			cout << "Sorting run in memory." << endl;

			sort(inputmm, inputmm + mappingSize/sizeof(unsigned long));

			string outfilename = getOutfilename(run);

			cout << "Writing sorted run into temporary file: " << outfilename << endl;

			int fdRun = open(outfilename.c_str(), O_CREAT|O_TRUNC|O_WRONLY, S_IRUSR|S_IWUSR);

			write(fdRun, inputmm, mappingSize);
			cout << "Wrote " << mappingSize / sizeof(unsigned long) << " ulongs to outfile." << endl;

			//cout << "first ulong: "<<inputvector[0]<<endl;

			if (munmap(inputmm, mappingSize) != 0) {
				cerr << "Cannot unmap file-memory." << endl;
				return;
			}
			cout << "Freed memory mapping." << endl;

		}
		cout << "Partitioning done." << endl;

		// merge partitions
		cout << "Running merge sort on " << runCount << " runs." << endl;

		// init priority queue
		priority_queue<SORTENTRY> queue;

		int* fdRunfiles = new int[runCount];
		// init runCount input file handles
		for (int run = 0; run < runCount; run++) {
			fdRunfiles[run] = open(getOutfilename(run).c_str(), O_RDONLY);
			unsigned long value;
			read(fdRunfiles[run], &value, sizeof(unsigned long));
			SORTENTRY entry = { run, value };
			queue.push(entry);
		}

		while(!queue.empty()) {
			SORTENTRY top = queue.top();
			write(fdOutput, &(top.value), sizeof(unsigned long));
			//cout<<"Queue contains: " << top.value << " from run #" << top.originRun << endl;
			queue.pop();
			int fdOrigin = fdRunfiles[top.originRun];
			unsigned long value;
			if (read(fdOrigin, &value, sizeof(unsigned long)) == 0) {
				// EOF
				close(fdOrigin);
				cout << "Merged all data from run #"<<top.originRun<<endl;
				remove(getOutfilename(top.originRun).c_str());
				continue;
			}
			SORTENTRY entry = { top.originRun, value };
			queue.push(entry);
		}
		cout << "Merging done." << endl;
	}
}

