/*
 * external_sort.h
 *
 *  Created on: Apr 23, 2012
 *      Author: dev
 */

#ifndef EXTERNAL_SORT_H_
#define EXTERNAL_SORT_H_

#include <fcntl.h>

// sorts size 64bit unsigned integers of data in fdInput using external merge sort and memSize bytes memory.
void externalSort(int fdInput, unsigned long size, int fdOutput, unsigned long memSize);


#endif /* EXTERNAL_SORT_H_ */
