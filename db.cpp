//============================================================================
// Name        : DB.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C, Ansi-style
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "util/external_sort.h"

using namespace std;

int oldmain(void) {
	cout << "MicroDB starting up." << endl;
	cout << "-----------------------" << endl;
	cout << "(c) 2012 Alexander Ried" << endl;
	cout << "-----------------------" << endl;


	externalSort(0, 1024, 0, 1024);


	cout << "Shutting down MicroDB." << endl;
	return EXIT_SUCCESS;
}

