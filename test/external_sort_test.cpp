/*
 * external_sort_test.cpp
 *
 *  Created on: Apr 23, 2012
 *      Author: dev
 */

#include "../util/external_sort.h"
#include <unistd.h>
#include <iostream>
#include <errno.h>
#include <string.h>
#include <ctime>

using namespace std;

int main(void) {
	clock_t start = clock();

	unsigned long size = 131072; // unsigned longs
	unsigned long memSize = 4*1024; // bytes of memory to use

	// allocate input and output files
	int fdInput = open("50mb", O_RDONLY);
	if (fdInput == -1) {
		cout << "Failed to open input file: " << strerror(errno) << endl;
		return -1;
	}

	int fdOutput = open("testout", O_CREAT|O_TRUNC|O_WRONLY, S_IRUSR|S_IWUSR);
	if (fdOutput == -1) {
		cout << "Failed to open output file: " << strerror(errno) <<endl;
		return -1;
	}

	int ret;
	if ((ret = posix_fallocate(fdOutput, 0, size*sizeof(unsigned long))) != 0)
			std::cerr << "warning: could not allocate file space: " << strerror(ret) << std::endl;

	//lseek(fdOutput, size -1, SEEK_SET);
	//write(fdOutput, "", 1);

	// externalSort aufrufen
	externalSort(fdInput, size, fdOutput, memSize);

	cout << "External sorting done in "<< double(clock()-start)/CLOCKS_PER_SEC <<"s." <<endl;

	// close filehandles
	close(fdInput);
	close(fdOutput);

}

